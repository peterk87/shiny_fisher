### Shiny Fisher: a utility for molecular marker discovery (MMD) from genomics datasets 

Determine statisically significant markers in your datasets using the Fisher's Exact Test with multiple test corrections like the False Discovery Rate (FDR). 

Simply upload a file with your binary (1/0) presence/absence data and a corresponding file with your sample information or metadata. 

