
NA_STR <- '!N/A'

qualitative_colours <- function(n, light=FALSE)
{
  # Get a specified number of qualitative colours if possible.
  # This function will default to a continuous color scheme if there are more
  # than 21 colours needed.

  # rainbow12equal <- c("#BF4D4D", "#BF864D", "#BFBF4D", "#86BF4D", "#4DBF4D", "#4DBF86", "#4DBFBF", "#4D86BF", "#4D4DBF", "#864DBF", "#BF4DBF", "#BF4D86")
  rich12equal <- c("#000040", "#000093", "#0020E9", "#0076FF", "#00B8C2", "#04E466", "#49FB25", "#E7FD09", "#FEEA02", "#FFC200", "#FF8500", "#FF3300")

  # Qualitative colour schemes by Paul Tol
  if(n >= 19 & n <= 21)
    # return 21 qualitative color scheme
    return(colorRampPalette(c("#771155", "#AA4488", "#CC99BB", "#114477", "#4477AA", "#77AADD", "#117777", "#44AAAA", "#77CCCC", "#117744", "#44AA77", "#88CCAA", "#777711", "#AAAA44", "#DDDD77", "#774411", "#AA7744", "#DDAA77", "#771122", "#AA4455", "#DD7788"))(n))
  if(n >= 16 & n <= 18)
    # up to 18 qualitative color scheme
    return(colorRampPalette(c("#771155", "#AA4488", "#CC99BB", "#114477", "#4477AA", "#77AADD", "#117777", "#44AAAA", "#77CCCC", "#777711", "#AAAA44", "#DDDD77", "#774411", "#AA7744", "#DDAA77", "#771122", "#AA4455", "#DD7788"))(n))
  if(n == 15)
    # 15 qualitative color scheme
    return(colorRampPalette(c("#114477", "#4477AA", "#77AADD", "#117755", "#44AA88", "#99CCBB", "#777711", "#AAAA44", "#DDDD77", "#771111", "#AA4444", "#DD7777", "#771144", "#AA4477", "#DD77AA"))(n))
  if(n > 12 & n <= 14)
    # 14 qualitative color scheme
    return(colorRampPalette(c("#882E72", "#B178A6", "#D6C1DE", "#1965B0", "#5289C7", "#7BAFDE", "#4EB265", "#90C987", "#CAE0AB", "#F7EE55", "#F6C141", "#F1932D", "#E8601C", "#DC050C"))(n))
  if(n > 9 & n <= 12)
    if(light)
      return(RColorBrewer::brewer.pal(n=n, name='Set3'))
    else
      return(RColorBrewer::brewer.pal(n=n, name='Paired'))
  
  if(n <= 9)
    if(light)
      return(RColorBrewer::brewer.pal(n=n, name='Pastel1'))
    else
      return(RColorBrewer::brewer.pal(n=n, name='Set1'))
  # else(n > 21,
  # If there are more than 21 qualitative colours, default to a continuous 
  # colour scheme, rich12equal in this case 
  colorRampPalette(rich12equal)(n)
}


get_df_col_names <- function(df) 
{
  cols <- colnames(df)
  names(cols) <- paste0(cols, ' [N distinct=', sapply(cols, function(x) {length(unique(df[[x]]))}), ']')
  cols
}

get_cat_group_names <- function(df, category, include_na=FALSE) 
{
  groups <- df[,category]
  if(is.list(groups))
    groups <- unlist(groups)
  groups <- sort(groups, na.last=TRUE)
  if(include_na)
  {
    groups[is.na(groups)] <- NA_STR
  }
  else
  {
    groups <- groups[!is.na(groups)]
  }
  unique_groups <- unique(groups)
  if (length(unique_groups) < nrow(df))
  {
    df_table <- as.data.frame(table(groups))
    groups <- as.vector(df_table[,1])
    group_sizes <- as.vector(df_table[,2])
    names(groups) <- mapply(function(x, y) 
      {
        paste0(x, ' [n=', y, ']')
      },
      groups,
      group_sizes)
    size_order <- order(-group_sizes)
    groups <- groups[size_order]
  }
  else
  {
    groups <- unique_groups
    groups <- sort(groups)
  }
  groups
}

get_numeric_columns <- function(df, complete_data=FALSE)
{
  # get the classes of each column, i.e. whether column contains data of type 
  # character, numeric, etc
  is_numeric_column <- sapply(colnames(df), function(x) is.numeric(df[,x]))
  
  numeric_columns <- names(is_numeric_column[is_numeric_column == TRUE])

  # are columns with only complete data requested?
  if(complete_data)
  {
    # get numeric columns with complete data; no missing values (NA or NULL)
    is_complete_column <- sapply(numeric_columns, 
      function(x) all(!is.na(df[,x])) & all(!is.null(df[,x])))
    numeric_columns <- names(is_complete_column[is_complete_column == TRUE])
  }

  numeric_columns
}

is_valid_input <- function(x)
{
  if(is.null(x)) return(FALSE)
  len_x <- length(x)
  if(len_x == 0) return(FALSE)
  if(len_x == 1)
  {
    if(is.na(x)) return(FALSE)
  }
  else
  {
    if(all(is.na(x))) return(FALSE)
  }

  if(is.data.frame(x))
  {
    if(ncol(x) == 0) return(FALSE)
    if(nrow(x) == 0) return(FALSE)
    return(TRUE)
  }
  if(is.character(x))
  {
    if(length(x) == 1)
    {
      if(x == '')
        return(FALSE)

      return(TRUE)
    }
    return(TRUE)
  }
  if(is.numeric(x))
  {
    if(is.nan(x)) return(FALSE)
    return(TRUE)
  }
  TRUE
}

get_scatterplot_colours <- function(vals
  ,quantitative=TRUE
  ,selected_groups=NULL
  ,log_scale=TRUE)
{
  if (quantitative) #continuous numerical
  {
    if(log_scale)
    {
      vals <- log10(vals)
      vals[vals == -Inf] <- 1000
      vals[vals == 1000] <- min(vals)
    }
    
    min_val <- min(vals, na.rm=TRUE)
    max_val <- max(vals, na.rm=TRUE)
    # if the min value is the same as the max value then there is only one
    # value in the list of values for a quanititative colour scheme
    if(min_val == max_val)
    {
      group_colours <- 'black'
      group_labels <- selected_groups
      point_colours <- rep('black', length(vals))
      return(list(group_colours=group_colours
        ,group_labels=group_labels
        ,point_colours=point_colours)) 
    }
    group_colours <- rgb(colorRamp(brewer.pal(n=11, name='Spectral'))(0:10/10)/255)
    group_labels <- if(log_scale)
      {
        sprintf('%.1f',10^(((max_val-min_val)/10) * 0:10 + min_val))
      }
      else
      {
        sprintf('%.1f',((max_val-min_val)/10) * 0:10 + min_val)
      }
    is_NA <- is.na(vals)
    scaled_vals <- (vals - min_val)/(max_val-min_val)
    scaled_vals[is_NA] <- 0
    point_colours <- rgb(colorRamp(brewer.pal(n=11, name='Spectral'))(scaled_vals)/255)
    point_colours[is_NA] <- '#00000000'
  }
  else # qualitative/categorical
  {
    qual_colours <- qualitative_colours(length(selected_groups))
    point_colours <- vals
    point_colours[!(point_colours %in% selected_groups | is.na(point_colours))] <- '#00000000'
    count <- 1
    while (count <= length(selected_groups))
    {
      if (selected_groups[count] == NA_STR)
      {
        point_colours[is.na(point_colours)] <- qual_colours[count]
      }
      else
      {
        point_colours[point_colours == selected_groups[count]] <- qual_colours[count]
      }

      count <- count + 1
    }
    group_colours <- qual_colours[1:length(selected_groups)]
    group_labels <- selected_groups
    point_colours <- point_colours
  }
  list(group_colours=group_colours
    ,group_labels=group_labels
    ,point_colours=point_colours
  )
}