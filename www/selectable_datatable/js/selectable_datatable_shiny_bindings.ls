/**
 * Object that contains a list of data tables that can send data to R
 * @type {Object}
 */
dataTables = {}
window.dataTables = dataTables

/**
 * Output binding that allows R to send data to the JavaScript code
 * Creates the selection data table (using the specialDataTable class) and adds it to an array of data tables
 * @type {Shiny}
 */
selectable_datatable_output_binding = new Shiny.OutputBinding!
$.extend selectable_datatable_output_binding,
  find: (scope) -> $ scope .find \.selectable_datatable
  onValueError: (el, err) !->
  renderValue: (el, data) !->
    return unless data?
    return unless data.cols?
    return unless data.dt_row_array?
    
    window.data := data

    if el.id of dataTables
      dt = dataTables[el.id]
      dt = new SelectableDataTable el, data.dt_row_array, data.cols
      dataTables[el.id] = dt
    else
      dt = new SelectableDataTable el, data.dt_row_array, data.cols
      dataTables[el.id] = dt


Shiny.outputBindings.register selectable_datatable_output_binding


/**
 * Input binding that sends the filtered selection data from the JavaScript data table to R so that R can make use of the data
 * @type {Shiny}
 */
selectable_datatable_input_binding = new Shiny.InputBinding!
$.extend selectable_datatable_input_binding,
  find: (scope) -> $ scope .find \.selectable_datatable
  getValue: (el) -> 
    if el.id of dataTables
      dt = dataTables[el.id]
      dt.get_filtered_selection_data!
    else
      []

  setValue: (el, value) !-> 
    if el.id of dataTables
      dt = dataTables[el.id]
      dt.set_filtered_selection_data value

  subscribe: (el, callback) !->
    $ el .on \change (e) !-> 
      callback!

Shiny.inputBindings.register selectable_datatable_input_binding
