export class SelectableDataTable

  /**
   * Array of selected strains from the data table
   * @type {Array}
   * @private
   */
  _selected: []

  /**
   * Contains the data used for filtering the data table
   * @type {Array}
   * @private
   */
  _filtered_data: []

  /**
   * Holds a copy of the data table's values
   * @type {Array}
   * @private
   */
  _dTable: []

  /**
   * Array of the selcted strains after all filtering has been applied to the data table
   * @type {Array}
   * @private
   */
  _filtered_selection_data: []


  /**
   * Class that creates a data table using the dataTables.js library
   * @param {String} el_id Id of the element that is creating the data table
   * @param {Element} el Element that is creating the data table
   * @param table_data Data that is either an HTML table (is_html == true) or 2D array of values 
   *         for the creation of the HTML table (may be either an Array(row data) or a String(HTML))
   * @param {Array} cols Column names used for the creation of the HTML table
   * @param {Boolean} is_html Determines whether the table_data variable is a HTML table or not
   * @constructor
   */
  (@_el, table_data, cols, is_html = false) ->
    /**
     * Used to store the correct scope for the value of 'this'
     * The variable '_this' is used so that the functions within the constructor can access the expected value of 'this'
     * @type {Node}
     */
    _this = @
    el_id = @_el.id
    @_cols := cols
    @_table_data := table_data
    _this._selected := []
    _this._filtered_data := []

    console.log 'ctor', @_el, table_data, cols, is_html
    console.log 'ctor this', @
    console.log @create_html_table
    /**
     * Check if an HTML table is given, if so, then use the HTML table and if not use a 2D matrix / array to create the HTML table
     */
    if is_html
      table_html = table_data
    else
      table_html = @create_html_table cols, table_data
    
    @$el = $ @_el 

    @$el.html table_html



    /**
     * Settings that will be used in the creation of the data table
     * @type {Object}
     */
    settings = do
      destroy: true
      lengthMenu: [5, 10, 20, 30, 50]
      pageLength: 10
      orderClasses: false
      order: []
      paging: true
      pagingType: 'simple_numbers'
      processing: true
      dom: 'T<"clear">lrtip'
      # User row selection
      tableTools: do
        sRowSelect: \multi
        aButtons: 
          * sExtends: 'select_all' 
          * sExtends: 'select_none'
        /**
         * Handles the row selection for the data table
         * @param {Node} Row that was selected
         */
        fnRowSelected: !~>
          @_handle_row_selection!
        /**
         * Handles the row deselection for the data table
         * @param {Node} Row that was deselected
         */
        fnRowDeselected: !~>
          @_handle_row_selection!
      scrollCollapse: true
      autoWidth: false

    /**
     * The data table
     * @type {Node}
     */
    table = $("\##{el_id} table").DataTable do
      settings
    @table := table

    @_tt := @table.tabletools!

    # Column filtering
    /**
     * Used as a name for '$("\##{el_id} table tfoot input")'
     * @type {Node}
     */
    filter_inputs = @$el.find "table tfoot input"

    @_filtered_inputs := filter_inputs

    /** Event triggered on keyup */
    filter_inputs.keyup !->
      console.log 'filter_inputs.keyup this', @, @.value, filter_inputs.index @
      table.column filter_inputs.index @
        .search @.value, true, false, false
        .draw!
      filteredRows = table.$ \tr, do
        filter: \applied
      console.log 'filteredRows', filteredRows.length, filteredRows
      # _this._filtered_data := []
      # $.each filteredRows, (index, tr) !->
      #   _this._filtered_data.push tr.firstChild.innerHTML
      
      console.log 'filter_inputs.keyup _this.table.data!', _this.table.data!
      selection = [0 til _this.table.data!.length]
      for i til _this._cols.length
        col_selection = []
        filter_value = filter_inputs[i].value
        if filter_value == ''
          continue
        _this.table.column i
          .data!
          .filter (v,idx) ->
            if v.match(filter_value) != null 
              col_selection.push idx
            return false
        console.log 'counts', count_selected, count_total
        if col_selection.length > 0 
          console.log 'filter_inputs.keyup Selection', i, filter_value, selection.length, col_selection.length
          selection = _.intersection selection, col_selection
      console.log 'filter_inputs.keyup FINAL Selection', selection
      _this._filtered_data := selection
      _this._filtered_selection_data := _.intersection selection, _this._selected
      console.log '_this._filtered_selection_data ', _this._filtered_selection_data 

      $ _this._el .trigger \change
    
    /** Event triggered when the textbox receives focus */
    filter_inputs.focus !->
      if @.className == "search_init"
        @.className = ""
        @.value = ""


    # Helper functions
    
    # End helper functions

    jQuery '<p class="selected_status">0 items selected</p>'
      .prependTo "\##{@_el.id} div.DTTT_container"

    $ @_el .on \change !~>
      selected = @get_filtered_selection_data!
      p = $ "\##{@_el.id} p.selected_status"
      p.text "#{selected.length} items selected"

    sel_btns = $ @_el .find \.DTTT_button
      .addClass "btn"

    select_all_btn = sel_btns.find("span:contains('Select all')").parent()

    select_all_btn.find \span
      .text ' Select All'

    jQuery '<i/>' class: "fa fa-list fa-lg"
      .prependTo select_all_btn


    clear_selection_btn = sel_btns.find("span:contains('Deselect all')").parent()

    clear_selection_btn.find \span
      .text ' Clear Selection'

    jQuery '<i/>' class: "fa fa-undo fa-lg"
      .prependTo clear_selection_btn


    $ @_el .find \.paginate_button
      .addClass "btn"

  /**
   * Contains the code for handleing the row selection using the TableTools 
   * extension. This function is contained within the scope of the contructor 
   * in order to be able to have direct access to the variable '_this'
   * @param {Node} dt Used for getting the row selection data from the data 
   * table
   */
  _handle_row_selection: !->
    @_selected := @_tt.fnGetSelectedIndexes!
    # $.each dt.fnGetSelectedData!, (i, x) !~>
    #   @_selected.push x[0]

    if @_filtered_data.length == 0
      @_filtered_selection_data := @_selected
    else
      @_filtered_selection_data := _.intersection @_selected, @_filtered_data
    console.log '_handle_row_selection @_filtered_selection_data ', @_filtered_selection_data 
    $ @_el .trigger \change

  /**
   * Creates a HTML table when given the row data and column names for the table
   * @param {Array} col_names Column names for the HTML table
   * @param {Array} row_data Values that are to be inserted into the HTML table
   * @return {String} The HTML table
   */
  create_html_table: (col_names, row_data) ->
    """
      <table class=\"display cell-border\">
        <thead>
          <tr>
            #{@get_table_headers(col_names)}
            <!-- <th>Edit</th> --!>
          </tr>
        </thead>
        <tfoot>
          <tr>
            #{@get_table_search_inputs(col_names)}
          </tr>
        </tfoot>
        <tbody>
          #{@get_table_data(col_names, row_data)}
        </tbody>
      </table>
    """

  /**
   * Formats the row data into a format that can be inserted into the HTML table
   * @param {Array} col_names Column names which are used for indexing the row_data array
   * @param {Array} row_data Values that will be inserted into the body of the HTML table
   * @return {String} HTML code coresponding to the body of the HTML table
   * @private
   */
  get_table_data: (col_names, row_data) ->
    table_data_html = ""
    if typeof col_names == 'string'
      table_data_html += "<tr><td>#{row_data[col_names]}</td></tr>"
    else
      for row_datum in row_data
        table_data_html += \<tr>
        for col_name in col_names
          d = row_datum[col_name]
          d = if d? then d else ''
          table_data_html += "<td>#{d}</td>"
        table_data_html += \</tr>
    return table_data_html

  /**
   * Formats the column names into a format that can be insserted into the HTML table
   * @param {Array} col_names Column names that will be inserted into the header of the HTML table
   * @return {String} HTML code coresponding to the header of the HTML table
   * @private
   */
  get_table_headers: (col_names) ->
    header_html = ""
    if typeof col_names == 'string'
      header_html += "<th>#{col_names}</th>"
    else
      for col_name in col_names
        header_html += "<th>#{col_name}</th>"
    return header_html

  /**
   * Generates the HTML code for adding text boxes to the footer of the HTML table
   * @param {Array} col_names Column names used for giving placeholder text to the text input boxes
   * @return {String} HTML code corresponding to the footer of the HTML table
   * @private
   */
  get_table_search_inputs: (col_names) ->
    search_inputs_html = ""
    for col_name in col_names
      search_inputs_html += """
        <th rowspan='1' colspan='1'>
          <input type='text' name='search_#{col_name}' placeholder='#{col_name}' class='search_init'>
        </th>
        """
    return search_inputs_html

  /**
   * Sets the value of _editable
   * @param {Boolean} editable Value to be assigned to the corresponding class variable
   */
  set_editable: (editable) ->
    @_editable := editable

  /**
   * Sets the value of _filtered_selection_data
   * @param {Array} filtered_selection_data Value to be assigned to the corresponding class variable
   */
  set_filtered_selection_data: (filtered_selection_data) ->
    @_filtered_selection_data := filtered_selection_data

  /**
   * Returns the value stored in the variable _selected
   * @return {Array} Value stored in _selected
   */
  get_selected: ->
    @_selected

  /**
   * Returns the value stored in the variable _filtered_data
   * @return {Array} Value stored in _filtered_data
   */
  get_filtered_data: ->
    @_filtered_data

  /**
   * Returns the value stored in the variable _dTable
   * @return {Array} Value stored in _dTable
   */
  get_dTable: ->
    @_dTable

  /**
   * Returns the value stored in _filtered_selection_data
   * @return {Array} Value stored in _filtered_selection_data
   */
  get_filtered_selection_data: ->
    @_filtered_selection_data
