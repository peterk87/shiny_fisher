/*
Custom message handler for colorizing Selectize.js select input items

*/
Shiny.addCustomMessageHandler \shiny_colorize_selectize_opts_handler  (data) !->
  unless data?
    return 
  unless data.id?
    return
  unless data.colours?
    return  

  data_els = $ "select\##{data.id}+div div.item"

  for i from 0 til data_els.length
    data_el = data_els[i]
    colour = data.colours[i]
    data_el.style.backgroundImage = "linear-gradient(to bottom, \#ffffff, #{colour})"