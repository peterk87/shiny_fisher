


dynamites = {}

dynamite_input_binding = new Shiny.InputBinding!
$.extend dynamite_input_binding,
  find: (scope) -> $ scope .find \.dynamite_input
  getValue: (el) -> 
    if el.id of dynamites
      dynamite = dynamites[el.id]
      dynamite.get_final_selection!
    else
      []

  receiveMessage: (el, data) !-> 
    console.log 'receiveMessage', el.id, data?.key
    return unless data?
    return unless data.dataframe?
    return unless data.key?


    if el.id of dynamites
      $ el .html ''
    
    dynamite = new Dynamite el, data.dataframe, data.key
    dynamites[el.id] = dynamite

  subscribe: (el, callback) !->
    $ el .on \change (e) !-> 
      callback!

Shiny.inputBindings.register dynamite_input_binding

